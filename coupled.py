from scipy import integrate
from scipy.interpolate import interp1d
import numpy as np
from matplotlib import pyplot as plt
import multiprocessing
from matplotlib import cm

plt.style.use('style.yml')

# dphis_mod
foundamental_res = True
alpha1 = 50
inits = [[0.,0.8, 1,1.2]]
M = 1
alpha2s = alpha1 + np.linspace(-1, 1, 24)
epss = np.linspace(0.005, 0.25, 24)
dphis_file = 'dphis_mod.npy'
toll = np.e**-7

# dphis_mod_1
# foundamental_res = True
# alpha1 = 50
# inits = [[0.,0.8, 1,1.2]]
# M = 1
# alpha2s = alpha1 + np.linspace(-3, 3, 32)
# epss = np.linspace(0.005, 0.75, 32)
# dphis_file = 'dphis_mod_1.npy'
# toll = np.e**-9

# dphis_mod_1_2    # non viene bene per come hai scelto beta = beta1 = beta2 vabbe
# foundamental_res = False
# alpha1 = 50
# inits = [[0.,0.8, 1,1.2]]
# M = 2
# alpha2s = M*alpha1 + np.linspace(-1, 1, 4)
# epss = np.linspace(0.005, 0.25, 4)
# dphis_file = 'dphis_mod_1_2.npy'
# toll = np.e**-7

def f2(t,y, e, alpha2):            # y = [x1,y1,x2,y2]
    return np.array([ y[0]-alpha1*y[1] - (y[0]-y[1])*(y[0]**2+y[1]**2)  +e*( y[2]-y[0] -y[3]+y[1] ),  \
                      alpha1*y[0]+y[1] - (y[0]+y[1])*(y[0]**2+y[1]**2)  +e*( y[2]-y[0] +y[3]-y[1] ),  \
                      y[2]-alpha2*y[3] - (y[2]-y[3])*(y[2]**2+y[3]**2)  +e*( -y[2]+y[0] +y[3]-y[1] ), \
                      alpha2*y[2]+y[3] - (y[2]+y[3])*(y[2]**2+y[3]**2)  +e*( -y[2]+y[0] -y[3]+y[1] ), \
                    ])

def run(alpha_eps_init):
    alpha2, eps, init = alpha_eps_init
    OMEGA = (alpha1+alpha2)/2-1 if foundamental_res else alpha1-1
    interval = ( 0, min(20./abs(4*eps-abs(alpha2-M*alpha1+(M-1))), 500) )
    print(f'alpha1={alpha1:.3f}\talpha2={alpha2:.3f}\teps={eps:.3f}\tt_end={interval[1]}')

    res = integrate.solve_ivp( lambda t,y: f2(t,y,eps, alpha2), t_span=interval, y0=init, method='RK45', rtol=1e-6, atol=1e-12, dense_output=True)
    if res.message != 'The solver successfully reached the end of the integration interval.':
        print(init, res.message)

    tt = np.linspace(interval[0], interval[1], 5000)            # dense_output True
    yy = res.sol(tt)                                            # yy = [ x1[tt], y1[tt], x2[tt], y2[tt] ]

    phi1_o = (np.arctan2(yy[1],yy[0])-(OMEGA*tt))%(2*np.pi)
    phi2_o = (np.arctan2(yy[3],yy[2])-M*(OMEGA*tt))%(2*np.pi)
    # dphi = phi1_o-phi2_o
    dphi = (np.arctan2(yy[1],yy[0])-(OMEGA*tt)-np.arctan2(yy[3],yy[2])+M*(OMEGA*tt))%(2*np.pi)

    return dphi

def check_conv(dphi, alpha2 ,eps):
    interval = ( 0, min(20./abs(4*eps-abs(alpha2-M*alpha1)), 500) )
    last_dphi = dphi[round(len(dphi)/2):]
    dt = (interval[1]-interval[0])/5000
    der = (last_dphi[1:]-last_dphi[:-1])/dt
    der = np.array([ d if abs(d)<0.5/dt else 0. for d in der ])

    m = abs(der.mean())
    print(m)

    return m

if __name__=="__main__":
    try:
        dphis = np.load(dphis_file)
    except OSError:
        dphis = np.zeros((len(alpha2s), len(epss), 5000))

        for init in inits:
            with multiprocessing.Pool(processes=None) as p:
                temp = p.map(run, [[alpha2s[i],epss[j],init] for i in range(len(alpha2s)) for j in range(len(epss))])

                ind = 0
                for i in range(len(alpha2s)):
                    for j in range(len(epss)):
                        dphis[i,j] = np.array(temp[ind])
                        ind+=1

        np.save(dphis_file, dphis)

    ms = np.zeros(dphis.shape[:2])
    conv = np.zeros(dphis.shape[:2])
    for i in range(ms.shape[0]):
        for j in range(ms.shape[1]):
            m = ms[i][j] = check_conv(dphis[i,j], alpha2s[i], epss[j])
            conv[i][j] = 1. if m < toll else 0.

    plt.hist(np.log(ms.flatten()), bins=20)
    plt.show()

    aa, ee = np.meshgrid(alpha2s, epss)
    plt.contourf(aa, ee, np.log(ms.T), levels=100)
    plt.colorbar()
    plt.plot(alpha2s, abs(alpha2s-M*alpha1)/4, color='red')
    plt.xlabel(r'$\alpha_2$')
    plt.ylabel(r'$\varepsilon$')
    plt.show()

    aa, ee = np.meshgrid(alpha2s, epss)
    plt.contourf(aa, ee, conv.T, levels=1, cmap=cm.RdYlGn)
    plt.plot(alpha2s, abs(alpha2s-M*alpha1)/4, color='black', linestyle='--')
    plt.xlabel(r'$\alpha_2$')
    plt.ylabel(r'$\varepsilon$')
    plt.show()
